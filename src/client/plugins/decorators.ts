import { createDecorator, VueDecorator } from 'vue-class-component';

export function Receive(event: string): VueDecorator {
  return createDecorator((component, key) => {
    if(!component.sockets) component.sockets = {};
    component.sockets[event] = function(...args) {
      this[key](...args);
    };
  });
}

export function Send(event: string): MethodDecorator {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return (target, key, descriptor: any) => {
    const original = descriptor.value;
    descriptor.value = function(...args): void {
      const emit = (value): void => this.$socket.emit(event, value);
      const returnValue = original(...args);
      if(returnValue instanceof Promise) returnValue.then(emit);
      else emit(returnValue);
    };
  };
}