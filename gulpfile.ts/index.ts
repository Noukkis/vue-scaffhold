import { log, dest } from './vars';
import { task, parallel, series } from 'gulp';
import del from 'del';

import './server';
import './client';

task('clean:log', async () => {
  await del(log);
});

task('clean:dist', async () => {
  await del(dest);
});

task('clean', parallel('clean:dist', 'clean:log'));

task('build', parallel('build:server', 'build:client'));
task('dev', parallel('dev:server', 'dev:client'));
task('cleanAndBuild', series('clean', 'build'));