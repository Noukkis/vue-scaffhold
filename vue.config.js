const HtmlWebpackPlugin = require('html-webpack-plugin');
const  { join } = require('path');
const  { displayName } = require('./package');

module.exports = {

  publicPath: '',
  productionSourceMap: false,
  outputDir: __dirname + '/dist/client',
  configureWebpack: {

    plugins: [
      new HtmlWebpackPlugin({
        title: displayName,
        template: 'src/client/index.html'
      })
    ],

    resolve: {
      alias: {
        '@': join(__dirname, '/src/client')
      }
    },
    entry: {
      app: './src/client/main.ts'
    }
  },

  devServer: {
    proxy: {
      '^/api': {
        target: 'http://localhost:3000',
        changeOrigin: true
      },
      '^/socket': {
        target: 'http://localhost:3000',
        ws: true,
        changeOrigin: true
      }
    }
  }
};
