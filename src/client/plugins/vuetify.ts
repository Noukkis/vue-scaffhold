import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import '@mdi/font/css/materialdesignicons.css';
import themes from '@/utils/themes';

Vue.use(Vuetify);

export default new Vuetify({ 
  theme: {
    themes,
    options: {
      customProperties: true,
    }
  } 
});