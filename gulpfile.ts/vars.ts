import { join } from 'path';

export const root = join(__dirname, '..');

export const Server = {
  src: join(root, 'src/server/'),
  dest: join(root, 'dist/server/')
};

export const Client = {
  src: join(root, 'src/client/'),
  dest: join(root, 'dist/client/')
};

export const log = join(root, 'log/');
export const dest = join(root, 'dist/');
export const tsconfig = join(root, 'tsconfig.json');