import { Server } from 'http';
import { Express } from 'express';
import { join } from 'path';
import app from './app';
import Ctrl from './controllers';
import Io from './io';
import logger from './logger';
import { Config, ConfigManager } from './config';
import { getEnvironment } from './utils';
import { displayName, version } from '../../package.json';

const CONFIG_FILE = join(__dirname, '../../config/config.yml');

start();

async function start(): Promise<void> {

  logger.info(`Starting ${displayName} v${version} for ${getEnvironment()}`);
  const config = await loadConfig();
  const ctrl = createCtrl(config);
  const app = createApp(ctrl);
  const server = createServer(app, ctrl);
  const io = createIo(ctrl, server);

  startServer(server, ctrl);
  startIo(io);
  startCtrl(ctrl);

}

async function loadConfig(): Promise<Config> {
  const manager = new ConfigManager(CONFIG_FILE);
  await manager.loadConfig();
  return manager.config;
}

function createCtrl(config: Config): Ctrl {
  return new Ctrl(config);
}

function createApp(ctrl: Ctrl): Express {
  return app(ctrl);
}

function createServer(app: Express, ctrl: Ctrl): Server {
  const server = new Server(app);
  const port = ctrl.getAppPort();
  server.on('error', (err) => onError(err, port));
  server.on('listening', () => onListening(port));
  return server;
}

function createIo(ctrl: Ctrl, server: Server): Io {
  return new Io(ctrl, server);
}

function startServer(server: Server, ctrl: Ctrl): void {
  const port = ctrl.getAppPort();
  server.listen(port);
}

function startIo(io: Io): void {
  io.start();
}

function startCtrl(ctrl: Ctrl): void {
  ctrl.start();
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error, port: number): void {
  if (error.syscall !== 'listen') {
    throw error;
  }
  
  switch (error.code) {
    case 'EACCES':
      logger.error(`Port ${port} requires elevated privileges`);
      process.exit(1);
      break;

    case 'EADDRINUSE':
      logger.error(`Port ${port} is already in use`);
      process.exit(1);
      break;

    default:
      throw error;
  }

}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening(port: number): void {
  logger.info('Listening on http://localhost:' + port);
}
