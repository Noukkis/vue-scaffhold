import { Config } from '../config';

export default class Ctrl {

  private readonly config: Config;

  public constructor(config: Config) {
    this.config = config;
  }

  public start(): void {

  }

  public getAppPort(): number {
    return this.config.port;
  }

}