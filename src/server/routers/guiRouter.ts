import express, { Router } from 'express';
import { join } from 'path';

const GUI_PATH = join(__dirname, '../../client');

export default function(): Router {
  const router = Router();
  
  router.use('/', express.static(GUI_PATH));

  return router;
}