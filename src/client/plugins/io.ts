import Vue from 'vue';
import store from '../store';
import io from 'socket.io-client';
import VueSocketIO from 'vue-socket.io';
 
const host = window.location.host;
const path = window.location.pathname + '../socket';
const socket = io(host, {path});

Vue.use(new VueSocketIO({
  connection: socket,
  vuex: {
    store,
    actionPrefix: 'SOCKET_'
  }
}));
