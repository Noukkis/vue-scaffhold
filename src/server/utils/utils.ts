import { promises as fs } from 'fs';
import mkdirp from 'mkdirp';
import { dirname } from 'path';
import { promisify } from 'util';

const mkdir = promisify(mkdirp);

export enum Environment {
  Dev = 'development',
  Prod = 'production'
}

export function getEnvironment(): Environment {
  const env = process.env.NODE_ENV;
  return (env == Environment.Dev)
    ? Environment.Dev
    : Environment.Prod;
}

export function fileExists(filepath): Promise<boolean> {
  return promise2Boolean(fs.access(filepath));
}

export async function writeFile(file, text): Promise<void> {
  const dir = dirname(file);
  await mkdir(dir);
  return fs.writeFile(file, text, 'utf8');
}

export async function readFile(file): Promise<string> {
  return fs.readFile(file, 'utf8');
}

export function promise2Boolean(promise: Promise<void>): Promise<boolean> {
  return new Promise((resolve) => {
    promise
      .then(() => resolve(true))
      .catch(() => resolve(false));
  });
}