import Home from '@/views/Home.vue';
import About from '@/views/About.vue';
import Playground from '@/views/Playground.vue';

export default [
  { path: '/', name: 'Home', icon: 'mdi-home', component: Home },
  { path: '/playground', name: 'Playground', icon: 'mdi-view-dashboard', component: Playground },
  { path: '/about', name: 'About', icon: 'mdi-information', component: About }
];