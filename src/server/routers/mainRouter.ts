import { Router } from 'express';

export default function(): Router {
  const router = Router();
  
  router.get('/', (req, res) => {
    res.redirect('/gui');
  });

  return router;
}