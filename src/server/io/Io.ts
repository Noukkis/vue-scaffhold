import { Server as HttpServer } from 'http';
import SocketIO, { Server as SocketServer, Socket } from 'socket.io';
import Ctrl from '../controllers';

const socketOptions = { serveClient: false, path: '/socket' };


export default class Io {

  private readonly ctrl: Ctrl;
  private readonly io: SocketServer

  public constructor(ctrl: Ctrl, server: HttpServer) {
    this.ctrl = ctrl;
    this.io = SocketIO(server, socketOptions);
  }

  public start(): void {
    this.io.on('connection', this.onConnection);
  }

  private onConnection(socket: Socket): void {
    socket.on('isUp', data => socket.emit('isUp', { time: Date.now() - data, title: 'Server up!' }));
  }

  public broadcast(channel, message): void {
    this.io.emit(channel, message);
  }

}