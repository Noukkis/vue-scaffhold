import { Client } from './vars';
import { task } from 'gulp';
import run from 'gulp-run-command';
import del from 'del';

task('clean:client', async () => {
  await del(Client.dest);
});

task('build:client', run('vue-cli-service build'));

task('dev:client', run('vue-cli-service serve'));