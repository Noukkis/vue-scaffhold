import { Server, tsconfig } from './vars';
import { task, parallel, src, dest } from 'gulp';
import { createProject } from 'gulp-typescript';
import nodemon from 'gulp-nodemon';
import del from 'del';

const tsProject = createProject(tsconfig);

task('build:server:code', () => {
  return src(Server.src + '**/*.ts')
    .pipe(tsProject())
    .js.pipe(dest(Server.dest));
});

task('build:server:views', () => {
  return src(Server.src + 'views/**/*')
    .pipe(dest(Server.dest + 'views'));
});

task('clean:server', async () => {
  await del(Server.dest);
});

task('build:server', parallel('build:server:code', 'build:server:views'));

task('dev:server', (done) => {
  nodemon({
    watch: [Server.src, 'config'],
    ext: 'ts, yml',
    exec: 'NODE_ENV=development ts-node ./src/server',
    done: done
  });
});