import createError from 'http-errors';
import express, { json, urlencoded, Express, Request, Response, NextFunction } from 'express';
import { join } from 'path';
import morgan from 'morgan';
import Youch from 'youch';
import Ctrl from '../controllers';
import { mainRouter, guiRouter, apiRouter } from '../routers';
import { Environment, getEnvironment } from '../utils';
import logger from '../logger';

export default function createApp(ctrl: Ctrl): Express {
  const app = express();
  setup(app, ctrl);
  return app;
}

function setup(app: Express, ctrl: Ctrl): void {
  setupGlobal(app, ctrl);
  setupMiddlewares(app);
  setupRoutes(app, ctrl);
  setupFallback(app);
}

function setupGlobal(app: Express, ctrl: Ctrl): void {
  const port = ctrl.getAppPort();
  app.set('port', port);
  app.set('views', join(__dirname, '../views'));
  app.set('view engine', 'pug');
}

function setupMiddlewares(app: Express): void {
  app.use(morgan('dev'));
  app.use(json());
  app.use(urlencoded({ extended: false }));
}

function setupRoutes(app: Express, ctrl: Ctrl): void {
  app.use('/', mainRouter());
  app.use('/gui', guiRouter());
  app.use('/api', apiRouter(ctrl));
}

function setupFallback(app: Express): void {

  app.use((req, res, next) => {
    next(createError(404));
  });

  app.use((err, req, res, next) => {
    if(!err.status) err.status = 500;
    res.status(err.status);
    next(err);
  }); 

  if(getEnvironment() == Environment.Dev) app.use(devErrorHandler);
  else app.use(prodErrorHandler);

}

async function devErrorHandler(err, req: Request, res: Response, _next: NextFunction): Promise<void> {
  const youch = new Youch(err, req);
  const html = await youch.toHTML();
  res.send(html);
}

function prodErrorHandler(err, req: Request, res: Response, _next: NextFunction): void {
  const title = 'Error ' + err.status;
  const status = err.status;
  const message = err.message;
  const options = { title, status, message };
  if(status >= 500) logger.error(err);
  res.render('error', options);
}