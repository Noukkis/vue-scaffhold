export default class Config {

  public port: number;

  public constructor() {
    this.port = 3000;
  }

}