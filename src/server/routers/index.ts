export { default as mainRouter } from './mainRouter';
export { default as guiRouter } from './guiRouter';
export { default as apiRouter } from './apiRouter';