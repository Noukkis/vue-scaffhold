import colors from 'vuetify/es5/util/colors';

const light = {

  primary: colors.blueGrey.base,
  secondary: colors.grey.darken1,
  accent: colors.teal.accent4,

  error: colors.red.base,
  info: colors.cyan.base,
  success: colors.green.base,
  warning: colors.orange.base,
};

const dark = {
  ...light
};

export default { light, dark };